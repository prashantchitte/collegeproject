package com.part1;

public class Address {
	private String line;
	private String city;
	private int zip;
	public Address() {
		super();
		
	}
	public Address(String line, String city, int zip) {
		super();
		this.line = line;
		this.city = city;
		this.zip = zip;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getZip() {
		return zip;
	}
	public void setZip(int zip) {
		this.zip = zip;
	}
	
	

}
