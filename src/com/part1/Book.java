package com.part1;

public class Book 
{
	private String bname;
	private int bid;
	private String Author;
	Department bdept;
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Department getBdept() {
		return bdept;
	}

	public void setBdept(Department bdept) {
		this.bdept = bdept;
	}

	public Book(String bname, int bid, String author, Department bdept) {
		super();
		this.bname = bname;
		this.bid = bid;
		Author = author;
		this.bdept = bdept;
	}

	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public int getBid() {
		return bid;
	}
	public void setBid(int bid) {
		this.bid = bid;
	}
	public String getAuthor() {
		return Author;
	}
	public void setAuthor(String author) {
		Author = author;
	}
	
	
}
