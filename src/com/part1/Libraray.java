package com.part1;

public class Libraray {
	Book[] book=null;
	private String lname;
	public Libraray() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Libraray(Book[] book, String lname) {
		super();
		this.book = book;
		this.lname = lname;
	}
	public Book[] getBook() {
		return book;
	}
	public void setBook(Book[] book) {
		this.book = book;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}

	
}
