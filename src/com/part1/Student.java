package com.part1;

public class Student {

	private int sid;
	private String sname; 
	Address sadd;
	Department sdept;
	
	public Student() {
		super();
		
	}
	
	public Student(int sid, String sname, Address sadd, Department sdept) {
		super();
		this.sid = sid;
		this.sname = sname;
		this.sadd = sadd;
		this.sdept = sdept;
	}

	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public Address getSadd() {
		return sadd;
	}
	public void setSadd(Address sadd) {
		this.sadd = sadd;
	}

	public Department getSdept() {
		return sdept;
	}

	public void setSdept(Department sdept) {
		this.sdept = sdept;
	}
	
}
