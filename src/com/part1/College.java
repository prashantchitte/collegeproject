package com.part1;

public class College {

	Department dept[]=null;
	Lecturer lect[]=null;
	Student stud[]=null;
	Libraray lib;
	Address address;
	
	public College(Department[] dept, Lecturer[] lect, Student[] stud, Libraray lib, Address address) {
		super();
		this.dept = dept;
		this.lect = lect;
		this.stud = stud;
		this.lib = lib;
		this.address = address;
	}
	public Department[] getDept() {
		return dept;
	}
	public void setDept(Department[] dept) {
		this.dept = dept;
	}
	public Lecturer[] getLect() {
		return lect;
	}
	public void setLect(Lecturer[] lect) {
		this.lect = lect;
	}
	public Student[] getStud() {
		return stud;
	}
	public void setStud(Student[] stud) {
		this.stud = stud;
	}
	public Libraray getLib() {
		return lib;
	}
	public void setLib(Libraray lib) {
		this.lib = lib;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	
	
	
}
