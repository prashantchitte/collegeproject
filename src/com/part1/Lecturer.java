package com.part1;

public class Lecturer {
	private int lid;
	private String lname;
	private int sal;
	Address laddr;
	Department ldept;
	
	public Lecturer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Lecturer(int lid, String lname, int sal, Address laddr, Department ldept) {
		super();
		this.lid = lid;
		this.lname = lname;
		this.sal = sal;
		this.laddr = laddr;
		this.ldept = ldept;
	}



	public Address getLaddr() {
		return laddr;
	}



	public void setLaddr(Address laddr) {
		this.laddr = laddr;
	}



	public Department getLdept() {
		return ldept;
	}



	public void setLdept(Department ldept) {
		this.ldept = ldept;
	}



	public int getLid() {
		return lid;
	}
	public void setLid(int lid) {
		this.lid = lid;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public int getSal() {
		return sal;
	}
	public void setSal(int sal) {
		this.sal = sal;
	}

}
