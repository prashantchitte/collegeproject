package com.part1;

public class Test
{
	
	public static void main(String[] args) {
		Address ad1=new Address("line1", "Nagar", 411040);
		Address ad2=new Address("line2", "pune", 411041);
		Address ad3=new Address("line3", "latur", 411042);
		
		
		Department d1=new Department("comp", 01);
		Department d2=new Department("it", 02);
		Department d3=new Department("mech", 03);
		
		Lecturer l1=new Lecturer(11, "Yogesh Sir", 99000, ad1, d1);
		Lecturer l2=new Lecturer(12, "Gopi", 40000, ad2, d2);
		Lecturer l3=new Lecturer(13, "Ramu", 50000, ad3, d3);
		
		Student s1=new Student(1001, "Prashant", ad1, d2);
		Student s2=new Student(1111, "Akshay", ad2, d1);
		Student s3=new Student(2222, "Sunil", ad3, d3);
		
		Book b1=new Book("Shapit", 01, "bhat", d3);
		Book b2=new Book("Rajhans", 02, "gandhi", d2);
		Book b3=new Book("Wede", 03, "sunil", d1);
		Book b4=new Book("Love", 04, "Prashant", d3);
		
		Book arr[]={b1,b2,b3,b4};
		Libraray lib1=new Libraray(arr, "universal");
		
		Department[] listOfDept = {d1,d2,d3};
		Student[] listOfStud ={s1,s2,s3};
		Lecturer[] listLect = {l1,l2,l3};
		
		College myCollege = new College(listOfDept, listLect, listOfStud, lib1, ad1);
		
		Book [] allBooks = myCollege.getLib().getBook();
		
		for (Book book : allBooks) {
			if(book.getAuthor().equals("sunil")){
				System.out.println(book.getAuthor());
				System.out.println(book.getBid());
				System.out.println(book.getBname());
				System.out.println(book.getBdept().getDname());
				
			}
		}
		
		
		
	}

}
